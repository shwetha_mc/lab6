#include "driver.h"
#include "mm.h"
#include "cuda_utils.h"

void
initCudaArray (dtype **d_A, dtype *h_A, unsigned int N)
{
	CUDA_CHECK_ERROR (cudaMalloc ((void**) d_A, N * sizeof (dtype)));
	CUDA_CHECK_ERROR (cudaMemcpy (*d_A, h_A, N * sizeof (dtype),
																cudaMemcpyHostToDevice));
}


__global__
void
mmSharedKernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	/* block indices */
	int bidx = blockIdx.x;
	int bidy = blockIdx.y;

	/* thread indices */
	int tidx = threadIdx.x;
	int tidy = threadIdx.y;

	/* row  index of first sub-block of matrix A processed by this thread block */
	int aStart = N * (BLOCK_SIZE * bidy);
	/* row  index of last sub-block of matrix A processed by this thread block */
	int aEnd   = aStart + N - 1;
	/* increment size for sub-block of matrix A */
	int aInc = BLOCK_SIZE;

	/* col index of first sub-blcok of matrx B processed by this thread block */
	int bStart = BLOCK_SIZE * bidx;
	/* last sub block is not needed since it'll have 1-on-1 match to A */
	/* increment size for sub-block of matrix B */
	int bInc = BLOCK_SIZE * N;

	/* temporary variable for accummulating the partial results */
	float cSub = 0;
	 
	/* Loop over the sub-matrices of A and B */
	for (int a = aStart, b = bStart; a <= aEnd; a += aInc, b += bInc) {
		/* declaration of shared memory for storing sub-block of A */
		__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];

		/* declaration of shared memory for storing sub-block of B */
		__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

		/* load the matrices from memory to shared memory */
		As[tidy][tidx] = A[a + N * tidy + tidx];
		Bs[tidy][tidx] = B[b + N * tidy + tidx];
		__syncthreads();

		/* multiply the two matrices together */
		/* one thread per element of C */
#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k)
			cSub += As[tidy][k] * Bs[k][tidx];

		/* synchornize before loading next sub-blocks */
		__syncthreads();
	}

	/* write back the results */
	int c = N * BLOCK_SIZE * bidy + BLOCK_SIZE * bidx;
	C[c + N * tidy + tidx] = cSub;

}
void
mmShared (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	

	mmSharedKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmSharedKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmSharedKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmSharedKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmSharedKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}



__global__
void
mmNaiveKernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	int i;
	dtype sum;
	int gidx = threadIdx.x + blockIdx.x * blockDim.x; /* column (j) */
	int gidy = threadIdx.y + blockIdx.y * blockDim.y; /* row (i) */
	int gid = gidx + gidy * N;

	sum = 0.0;
	for(i = 0; i < N; i++) {
		sum += A[gidy * N + i] * B[i * N + gidx];
	}
	C[gid] = sum;
}
void
mmNaive (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE);	


	mmNaiveKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmNaiveKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmNaiveKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmNaiveKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();


	mmNaiveKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}


__global__
void
mmShared2Kernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	/* insert your code here */
	/* block indices */
        int bidx = blockIdx.x;
        int bidy = blockIdx.y;

        /* thread indices */
        int tidx = threadIdx.x;
        int tidy = threadIdx.y;

        /* row  index of first sub-block of matrix A processed by this thread block */
        int aStart = N * (BLOCK_SIZE * bidy);
        /* row  index of last sub-block of matrix A processed by this thread block */
        int aEnd   = aStart + N - 1;
        /* increment size for sub-block of matrix A */
        int aInc = BLOCK_SIZE;

        /* col index of first sub-blcok of matrx B processed by this thread block */
        int bStart = BLOCK_SIZE * bidx;
        /* last sub block is not needed since it'll have 1-on-1 match to A */
        /* increment size for sub-block of matrix B */
        int bInc = BLOCK_SIZE * N;
	
        /* temporary variable for accummulating the partial results */
        float cSub = 0;
	float cSub2 = 0;
        /* Loop over the sub-matrices of A and B */
        for (int a = aStart, b = bStart; a <= aEnd; a += aInc, b += bInc) {
		/* declaration of shared memory for storing sub-block of A */
                __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];

                /* declaration of shared memory for storing sub-block of B */
                __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
//		__shared__ float As2[BLOCK_SIZE][BLOCK_SIZE/2];
                /* load the matrices from memory to shared memory */
//		if(tidy<(BLOCK_SIZE/2))
                As[tidy][tidx] = A[a + N * tidy + tidx];
//		else
//		{
		// if(bidx!=((N/BLOCK_SIZE)-1))
		 As[tidy+(BLOCK_SIZE/2)][tidx] = A[a + N * (tidy+(BLOCK_SIZE/2)) + tidx ];
//		}
	
                Bs[tidy][tidx] = B[b + N * tidy + tidx];
	        Bs[tidy+(BLOCK_SIZE/2)][tidx] = B[b + N * (tidy+(BLOCK_SIZE/2) )+ (tidx)];
		  __syncthreads();

                /* multiply the two matrices together */
                /* one thread per element of C */
#pragma unroll
			
                for (int k = 0; k < BLOCK_SIZE ; ++k)
                {
//			if(k<(BLOCK_SIZE/2))
		        cSub += As[tidy][k] * Bs[k][tidx];
		
			//if((k+(BLOCK_SIZE/2))<BLOCK_SIZE)
			cSub2 += As[tidy+(BLOCK_SIZE/2)][k] * Bs[k][tidx];
	
		}
                /* synchornize before loading next sub-blocks */
                __syncthreads();
        }

        /* write back the results */
        int c = N * BLOCK_SIZE * bidy + BLOCK_SIZE * bidx;
        C[c + N * tidy + tidx] = cSub;
	C[c + N * (tidy+(BLOCK_SIZE/2)) + tidx] = cSub2;

	
//	if(bidx!=(BLOCK_SIZE/N))
//	C[c + N * tidy + tidx+ (BLOCK_SIZE/2)] = cSub2;
	//printf ("BLOCK_SIZE: %d \n", tidy);
}
void
mmShared2 (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE / 2);	

	mmShared2Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared2Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared2Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared2Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared2Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}


__global__
void
mmShared4Kernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	 /* insert your code here */
        /* block indices */
        int bidx = blockIdx.x;
        int bidy = blockIdx.y;

        /* thread indices */
        int tidx = threadIdx.x;
        int tidy = threadIdx.y;

        /* row  index of first sub-block of matrix A processed by this thread block */

    int aStart = N * (BLOCK_SIZE * bidy);
        /* row  index of last sub-block of matrix A processed by this thread block */
        int aEnd   = aStart + N - 1;
        /* increment size for sub-block of matrix A */
        int aInc = BLOCK_SIZE;

        /* col index of first sub-blcok of matrx B processed by this thread block */
        int bStart = BLOCK_SIZE * bidx;
        /* last sub block is not needed since it'll have 1-on-1 match to A */
        /* increment size for sub-block of matrix B */
        int bInc = BLOCK_SIZE * N;
//	int numparts = 4;
        /* temporary variable for accummulating the partial results */
        float cSub = 0;
        float cSub2 = 0;
	float cSub3 = 0;
	float cSub4 = 0;
        /* Loop over the sub-matrices of A and B */
        for (int a = aStart, b = bStart; a <= aEnd; a += aInc, b += bInc) {
	 __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
         /* declaration of shared memory for storing sub-block of B */
         __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
		
		As[tidy][tidx] = A[a + N * tidy + tidx];
		As[tidy+(BLOCK_SIZE/4)][tidx] = A[a + N * (tidy+(BLOCK_SIZE/4))+ tidx ];
		As[tidy+(2*(BLOCK_SIZE/4))][tidx] = A[a + N * (tidy+(2*(BLOCK_SIZE/4))) + tidx ];
		As[tidy+(3*(BLOCK_SIZE/4))][tidx] = A[a + N * (tidy+(3*(BLOCK_SIZE/4))) + tidx ];
	        Bs[tidy][tidx] = B[b + N * tidy + tidx];
                Bs[tidy+(BLOCK_SIZE/4)][tidx] = B[b + N * (tidy+ (BLOCK_SIZE/4) )+ (tidx)];
                Bs[tidy+(2*(BLOCK_SIZE/4))][tidx] = B[b + N * (tidy+(2*(BLOCK_SIZE/4)) )+ (tidx)];
		Bs[tidy+(3*(BLOCK_SIZE/4))][tidx] = B[b + N * (tidy+(3*(BLOCK_SIZE/4)) )+ (tidx)];
	
		   __syncthreads();

                /* multiply the two matrices together */
                /* one thread per element of C */
#pragma unroll

                for (int k = 0; k < BLOCK_SIZE ; ++k)
                {
                        cSub += As[tidy][k] * Bs[k][tidx];
                        cSub2 += As[tidy+(BLOCK_SIZE/4)][k] * Bs[k][tidx];
			cSub3 +=  As[tidy+(2*(BLOCK_SIZE/4))][k] * Bs[k][tidx];
			cSub4 +=  As[tidy+(3*(BLOCK_SIZE/4))][k] * Bs[k][tidx];
                }
                /* synchornize before loading next sub-blocks */
                __syncthreads();
        }
		 /* write back the results */
        int c = N * BLOCK_SIZE * bidy + BLOCK_SIZE * bidx;
        C[c + N * tidy + tidx] = cSub;
        C[c + N * (tidy+(BLOCK_SIZE/4)) + tidx] = cSub2;
	 C[c + N * (tidy+(2*(BLOCK_SIZE/4))) + tidx] = cSub3;
	 C[c + N * (tidy+(3*(BLOCK_SIZE/4))) + tidx] = cSub4;


	
}
void
mmShared4 (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE / 4);	

	mmShared4Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared4Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared4Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared4Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared4Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}



__global__
void
mmShared8Kernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	
	int bidx = blockIdx.x;
        int bidy = blockIdx.y;

        /* thread indices */
        int tidx = threadIdx.x;
        int tidy = threadIdx.y;

        /* row  index of first sub-block of matrix A processed by this thread block */
        int aStart = N * (BLOCK_SIZE * bidy);
        /* row  index of last sub-block of matrix A processed by this thread block */
        int aEnd   = aStart + N - 1;
        /* increment size for sub-block of matrix A */
//        int aInc = BLOCK_SIZE;

        /* col index of first sub-blcok of matrx B processed by this thread block */
        int bStart = BLOCK_SIZE * bidx;
        /* last sub block is not needed since it'll have 1-on-1 match to A */
        /* increment size for sub-block of matrix B */
        //t bInc = BLOCK_SIZE * N;
        int numparts = 8;
        /* temporary variable for accummulating the partial results */
        float cSub,cSub2,cSub3,cSub4,cSub5,cSub6,cSub7,cSub8 = 0;
        /*float cSub2 = 0;
        float cSub3 = 0;
        float cSub4 = 0;
        float cSub5 = 0;
        float cSub6 = 0;
        float cSub7 = 0;
        float cSub8 = 0;*/
        //int i;
        /* Loop over the sub-matrices of A and B */
        for (int a = aStart, b = bStart; a <= aEnd; a += BLOCK_SIZE, b += (BLOCK_SIZE * N)) {
         __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
         /* declaration of shared memory for storing sub-block of B */
         __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
                for(int i=0; i<numparts ; ++i)
                {
                        As[tidy+i*(BLOCK_SIZE/numparts)][tidx] = A[a + N * (tidy+i*(BLOCK_SIZE/numparts)) + tidx ];
                        Bs[tidy+i*(BLOCK_SIZE/numparts)][tidx] = B[b + N * (tidy+i*(BLOCK_SIZE/numparts)) + tidx ];

                }
                __syncthreads();
#pragma unroll

                for (int k = 0; k < BLOCK_SIZE ; ++k)
                {
                        cSub += As[tidy][k] * Bs[k][tidx];


                        cSub2 += As[tidy+(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub3 +=  As[tidy+2*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub4 +=  As[tidy+3*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub5 +=  As[tidy+4*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub6 +=  As[tidy+5*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub7 +=  As[tidy+6*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub8 +=  As[tidy+7*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
        //               __syncthreads();
                }
                /* synchornize before loading next sub-blocks */
              __syncthreads();
       //
        //                cSub8 +=  As[tidy+7*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];

          //    }
                /* synchornize before loading next sub-blocks */
              //  __syncthreads();
        }
                int c = N * BLOCK_SIZE * bidy + BLOCK_SIZE * bidx;
        C[c + N * tidy + tidx] = cSub;
        C[c + N * (tidy+(BLOCK_SIZE/numparts)) + tidx] = cSub2;
         C[c + N * (tidy+(2*(BLOCK_SIZE/numparts))) + tidx] = cSub3;
         C[c + N * (tidy+(3*(BLOCK_SIZE/numparts))) + tidx] = cSub4;
        C[c + N * (tidy+(4*(BLOCK_SIZE/numparts))) + tidx] = cSub5;
        C[c + N * (tidy+(5*(BLOCK_SIZE/numparts))) + tidx] = cSub6;
        C[c + N * (tidy+(6*(BLOCK_SIZE/numparts))) + tidx] = cSub7;
        C[c + N * (tidy+(7*(BLOCK_SIZE/numparts))) + tidx] = cSub8;

}
void
mmShared8 (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE, BLOCK_SIZE / 8);	

	mmShared8Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared8Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared8Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared8Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmShared8Kernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}

__global__
void
mmMyOwnKernel (dtype* A, dtype* B, dtype* C, unsigned int N)
{

 	int bidx = blockIdx.x;
        int bidy = blockIdx.y;

        /* thread indices */
        int tidx = threadIdx.x;
        int tidy = threadIdx.y;

        /* row  index of first sub-block of matrix A processed by this thread block */
        int aStart = N * (BLOCK_SIZE * bidy);
        /* row  index of last sub-block of matrix A processed by this thread block */
        int aEnd   = aStart + N - 1;
        /* increment size for sub-block of matrix A */
        int aInc = BLOCK_SIZE;

        /* col index of first sub-blcok of matrx B processed by this thread block */
        int bStart = BLOCK_SIZE * bidx;
        /* last sub block is not needed since it'll have 1-on-1 match to A */
        /* increment size for sub-block of matrix B */
        int bInc = BLOCK_SIZE * N;
        int numparts = 8;
        /* temporary variable for accummulating the partial results */
        float cSub,cSub11=0;
	//cSub12,cSub13 = 0;
	 float cSub2,cSub21=0;
//cSub22,cSub23 = 0;
        float cSub3,cSub31=0;
//cSub32,cSub33 = 0;
        float cSub4,cSub41=0;
//cSub42,cSub43 = 0;
        float cSub5,cSub51=0;
//,cSub52,cSub53 = 0;
        float cSub6,cSub61=0;
//,cSub62,cSub63 = 0;
        float cSub7,cSub71=0;
//,cSub72,cSub73 = 0;
        float cSub8,cSub81=0;
//,cSub82,cSub83 = 0;
        int i;
        /* Loop over the sub-matrices of A and B */
        for (int a = aStart, b = bStart; a <= aEnd; a += aInc, b += bInc) {
         __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
         /* declaration of shared memory for storing sub-block of B */
         __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
                for(i=0; i<numparts ; ++i)
                {
			for(int j=0; j<2; ++j)
			{
                        As[tidy+i*(BLOCK_SIZE/numparts)][tidx+(j*BLOCK_SIZE/2)] = A[a + N * (tidy+i*(BLOCK_SIZE/numparts)) + (tidx+(j*BLOCK_SIZE/2 ))];
                        Bs[tidy+i*(BLOCK_SIZE/numparts)][tidx+(j*BLOCK_SIZE/2)] = B[b + N * (tidy+i*(BLOCK_SIZE/numparts)) + (tidx+(j*BLOCK_SIZE/2) )];
			}
                }
                __syncthreads();
		#pragma unroll

                for (int k = 0; k < (BLOCK_SIZE) ; ++k)
                {
                        cSub += As[tidy][k] * Bs[k][tidx];
			cSub11 += As[tidy][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			//cSub12 +=As[tidy][k+(BLOCK_SIZE/2)] * Bs[k+(BLOCK_SIZE/2)][tidx];
			//cSub13 +=As[tidy+(BLOCK_SIZE/2)][k+(BLOCK_SIZE/2)] * Bs [k+(BLOCK_SIZE/2)][tidx];
                        cSub2 +=  As[tidy+(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
			cSub21 += As[tidy+(BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			  cSub3 +=  As[tidy+(2*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub31 += As[tidy+(2*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
		  	cSub4 +=  As[tidy+(3*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub41 += As[tidy+(3*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			  cSub5 +=  As[tidy+(4*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub51 += As[tidy+(4*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			  cSub6 +=  As[tidy+(5*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub61 += As[tidy+(5*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			  cSub7 +=  As[tidy+(6*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub71 += As[tidy+(6*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];
			  cSub8 +=  As[tidy+(7*BLOCK_SIZE/numparts)][k] * Bs[k][tidx];
                        cSub81 += As[tidy+(7*BLOCK_SIZE/numparts)][k] * Bs [k][tidx+(BLOCK_SIZE/2)];

       //               __syncthreads();
                }
                /* synchornize before loading next sub-blocks */
              __syncthreads();
       //
        //                cSub8 +=  As[tidy+7*(BLOCK_SIZE/numparts)][k] * Bs[k][tidx];

          //    }
                /* synchornize before loading next sub-blocks */
              //  __syncthreads();
        }
                int c = N * BLOCK_SIZE * bidy + BLOCK_SIZE * bidx;
        C[c + N * tidy + tidx] = cSub;
	C[c + N * (tidy) + (tidx+BLOCK_SIZE/2)] = cSub11;
        C[c + N * (tidy+(BLOCK_SIZE/numparts)) + (tidx)] = cSub2;
	C[c + N * (tidy+(BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub21;
         C[c + N * (tidy+(2*BLOCK_SIZE/numparts)) + (tidx)] = cSub3;
	C[c + N * (tidy+(2*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub31;
	C[c + N * (tidy+(3*BLOCK_SIZE/numparts)) + (tidx)] = cSub4;
        C[c + N * (tidy+(3*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub41;	
	C[c + N * (tidy+(4*BLOCK_SIZE/numparts)) + (tidx)] = cSub5;
        C[c + N * (tidy+(4*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub51;
	C[c + N * (tidy+(5*BLOCK_SIZE/numparts)) + (tidx)] = cSub6;
        C[c + N * (tidy+(5*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub61;
	C[c + N * (tidy+(6*BLOCK_SIZE/numparts)) + (tidx)] = cSub7;
        C[c + N * (tidy+(6*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub71;
	C[c + N * (tidy+(7*BLOCK_SIZE/numparts)) + (tidx)] = cSub8;
        C[c + N * (tidy+(7*BLOCK_SIZE/numparts)) + (tidx+(BLOCK_SIZE/2))] = cSub81;
}
void
mmMyOwn (dtype* A, dtype* B, dtype* C, unsigned int N)
{
	unsigned int nBlocks;


	nBlocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

	dim3 grid (nBlocks, nBlocks);	
	dim3 block (BLOCK_SIZE /2, BLOCK_SIZE /8);	

	mmMyOwnKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmMyOwnKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmMyOwnKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmMyOwnKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
	mmMyOwnKernel <<<grid, block>>> (A, B, C, N);
	cudaThreadSynchronize ();
}


void cudaMM (dtype *A, dtype* B, dtype* C, unsigned int N, unsigned int OPT, dtype* h_C)
{
        cudaEvent_t start, stop;
        float elapsedTime;

	CUDA_CHECK_ERROR (cudaEventCreate (&start));
	CUDA_CHECK_ERROR (cudaEventCreate (&stop));

	fprintf (stderr, "Executing test case [%d]\n", OPT);
	fprintf (stderr, "[1]: Naive | [2]: shared memory| [3]: SM 2 per thread | [4]: SM 4 per thread | [5]: SM 8 per thread | [6]: my own implementation \n");

	
	CUDA_CHECK_ERROR (cudaEventRecord (start, 0));
	/* execute kernel */
	switch (OPT) {
		case 1:
			mmNaive (A, B, C, N);	
			break;
		case 2:
			mmShared (A, B, C, N);	
			break;
		case 3:
			mmShared2 (A, B, C, N);	
			break;
		case 4:
			mmShared4 (A, B, C, N);	
			break;
		case 5:
			mmShared8 (A, B, C, N);	
			break;
		case 6:
			mmMyOwn (A, B, C, N);
			break;
		default:
			mmNaive (A, B, C, N);	
	} 
	CUDA_CHECK_ERROR (cudaEventRecord (stop, 0));
	CUDA_CHECK_ERROR (cudaEventSynchronize (stop));
	CUDA_CHECK_ERROR (cudaEventElapsedTime (&elapsedTime, start, stop));
	elapsedTime = elapsedTime / 5;

	CUDA_CHECK_ERROR (cudaMemcpy (h_C, C, N * N * sizeof (dtype), 
																cudaMemcpyDeviceToHost));

	fprintf (stderr, "Execution time: %f ms\n", elapsedTime);
	fprintf (stderr, "Equivalent performance: %f GFLOP/s\n", 
						1e-6 * 2 * N * N * N / elapsedTime );

	CUDA_CHECK_ERROR (cudaEventDestroy (start));
	CUDA_CHECK_ERROR (cudaEventDestroy (stop));

}


